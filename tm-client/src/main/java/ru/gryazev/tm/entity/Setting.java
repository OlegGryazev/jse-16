package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Setting extends AbstractCrudEntity {

    @Nullable
    private String userId = null;

    @Nullable
    private String key;

    @Nullable
    private String value;

    public Setting(@NotNull String key, @Nullable String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public @Nullable String getUserId() {
        return userId;
    }

}
