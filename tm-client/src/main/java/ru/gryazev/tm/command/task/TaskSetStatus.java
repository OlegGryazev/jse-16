package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Status;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public class TaskSetStatus extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-set-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Set entered status to task.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final String projectId = getProjectId();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskEndpoint.getTaskId(token, projectId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        @Nullable final Task task = taskEndpoint.findOneTask(token, taskId);
        if (task == null) throw new CrudNotFoundException();
        @Nullable final Status status = terminalService.getStatus();
        if (status == null) throw new CrudUpdateException();
        task.setStatus(status);
        @Nullable final Task editedTask = taskEndpoint.editTask(token, task);
        if (editedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
