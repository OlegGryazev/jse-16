package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.User;


@NoArgsConstructor
public final class UserViewCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-view";
    }

    @Override
    public String getDescription() {
        return "View user data.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final String token = getToken();
        @Nullable final User user = serviceLocator.getUserEndpoint().findCurrentUser(token);
        if (user != null) terminalService.printUser(user);
    }

}
