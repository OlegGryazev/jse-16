package ru.gryazev.tm.command.task.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Setting;

@NoArgsConstructor
public class TaskSortStatus extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sort-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts tasks in order of task status.";
    }

    @Override
    public void execute() {
        if (terminalService == null || settingService == null) return;
        settingService.setSetting(new Setting("task-sort", "status"));
        terminalService.print("[OK]");
    }

}
