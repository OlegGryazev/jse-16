package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Setting;

public interface ISettingService extends IService<Setting> {

    @Nullable
    public String findValueByKey(@Nullable String key);

    @Nullable
    public Setting setSetting(@NotNull Setting setting);

}
