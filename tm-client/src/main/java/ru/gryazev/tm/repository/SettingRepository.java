package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ISettingRepository;
import ru.gryazev.tm.entity.Setting;

public class SettingRepository extends AbstractRepository<Setting> implements ISettingRepository {

    @Nullable
    @Override
    public String findValueByKey(@NotNull final String key) {
        @Nullable final Setting setting = findOneByKey(key);
        if (setting == null) return null;
        return setting.getValue();
    }

    @Nullable
    @Override
    public Setting merge(@NotNull final Setting setting) {
        return entities.put(setting.getId(), setting);
    }

    @Nullable
    public Setting persist(@NotNull final Setting setting) {
        if (entities.get(setting.getId()) != null) return null;
        return entities.put(setting.getId(), setting);
    }

    @Nullable
    @Override
    public Setting findOneByKey(@NotNull final String key) {
        return findAll().stream().filter(o ->
                key.equals(o.getKey())).findFirst().orElse(null);
    }

}
