package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;
    
    @NotNull
    @Override
    public List<Project> findAllProject(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        @NotNull final List<ProjectEntity> entities = serviceLocator.getProjectService().findAll();
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(ProjectEntity.toProjectDto(o)));
        return dtos;
    }

    @Nullable
    @Override
    public Project createProject(@Nullable final String token, @Nullable final Project project) throws Exception {
        if (serviceLocator == null || token == null || project == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @Nullable final ProjectEntity createdProjectEntity = serviceLocator.getProjectService()
                .create(session.getUserId(), Project.toProjectEntity(serviceLocator, project));
        return ProjectEntity.toProjectDto(createdProjectEntity);
    }

    @Nullable
    @Override
    public String getProjectId(@Nullable final String token, final int projectIndex) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getProjectService().getProjectId(session.getUserId(), projectIndex);
    }

    @Nullable
    @Override
    public Project findOneProject(@Nullable final String token, @Nullable final String entityId) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @Nullable final ProjectEntity projectEntity = serviceLocator.getProjectService()
                .findOne(session.getUserId(), entityId);
        return ProjectEntity.toProjectDto(projectEntity);
    }

    @NotNull
    @Override
    public List<Project> findProjectByUserId(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @NotNull final List<ProjectEntity> entities = serviceLocator.getProjectService().findByUserId(session.getUserId());
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(ProjectEntity.toProjectDto(o)));
        return dtos;
    }

    @Override
    public @NotNull List<Project> findProjectByUserIdSorted(@Nullable String token, @Nullable String sortType) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @NotNull final List<ProjectEntity> entities = serviceLocator.getProjectService().findByUserIdSorted(session.getUserId(), sortType);
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(ProjectEntity.toProjectDto(o)));
        return dtos;
    }

    @Nullable
    @Override
    public Project editProject(@Nullable final String token, @Nullable final Project project) throws Exception {
        if (serviceLocator == null || token == null || project == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @Nullable final ProjectEntity editedProjectEntity = serviceLocator.getProjectService()
                .edit(session.getUserId(), Project.toProjectEntity(serviceLocator, project));
        return ProjectEntity.toProjectDto(editedProjectEntity);
    }

    @NotNull
    @Override
    public List<Project> findProjectByDetails(@Nullable String token, @NotNull String projectDetails) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @NotNull final List<ProjectEntity> entities = serviceLocator.getProjectService().findByDetails(session.getUserId(), projectDetails);
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(ProjectEntity.toProjectDto(o)));
        return dtos;
    }

    @NotNull
    @Override
    public List<Project> findProjectByName(@Nullable String token, @Nullable String projectName) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        @NotNull final List<ProjectEntity> entities = serviceLocator.getProjectService().findByName(session.getUserId(), projectName);
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(ProjectEntity.toProjectDto(o)));
        return dtos;
    }

    @Override
    public void removeProject(@Nullable final String token, @Nullable final String entityId) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getProjectService().remove(session.getUserId(), entityId);
    }

    @Override
    public void removeAllProject(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }
    
    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
