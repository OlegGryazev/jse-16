package ru.gryazev.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class Constant {

    @NotNull
    public final static String SALT = UUID.randomUUID().toString();

    public final static int CYCLE_COUNT = 10;

    public final static long SESSION_LIFETIME = 3600000;

    public final static String DATA_DIR = System.getProperty("user.dir") + "/tm-server/data/";

}
