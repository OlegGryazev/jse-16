package ru.gryazev.tm.error;

public final class CrudNotFoundException extends CrudException {

    public CrudNotFoundException() {
        super("Error: element not found.");
    }

}
