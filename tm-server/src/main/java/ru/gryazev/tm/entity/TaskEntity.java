package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.Task;
import ru.gryazev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskEntity extends AbstractCrudEntity {

    @NotNull
    @ManyToOne
    private UserEntity user;

    @Nullable
    @ManyToOne
    private ProjectEntity project;

    @Nullable
    @Column(name = "task_name")
    private String name;

    @Nullable
    private String details;

    @Nullable
    @Column(name = "date_start")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    private Date dateFinish;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    @Column(name = "timestamp")
    private Long createMillis = new Date().getTime();

    @Nullable
    public static Task toTaskDto(@Nullable final TaskEntity taskEntity) {
        if (taskEntity == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskEntity.getId());
        task.setProjectId(taskEntity.getProject() == null ? null : taskEntity.getProject().getId());
        task.setName(taskEntity.getName());
        task.setDetails(taskEntity.getDetails());
        task.setDateStart(taskEntity.getDateStart());
        task.setDateFinish(taskEntity.getDateFinish());
        task.setStatus(taskEntity.getStatus());
        task.setUserId(taskEntity.getUser().getId());
        task.setCreateMillis(taskEntity.getCreateMillis());
        return task;
    }

}
