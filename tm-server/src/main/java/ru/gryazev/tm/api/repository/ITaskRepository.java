package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.TaskEntity;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskEntity> {

    @Nullable
    public TaskEntity findOneById(@NotNull String userId, @NotNull String id);

    public void removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    public List<TaskEntity> findAllByName(@NotNull String userId, @NotNull String taskName);

    @NotNull
    public List<TaskEntity> findAllByDetails(@NotNull String userId, @NotNull String taskDetails);

    @NotNull
    public List<TaskEntity> findAllByUserId(@NotNull String userId);

    @NotNull
    public List<TaskEntity> findAllByProjectIdSorted(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String sqlSortType
    );

    @NotNull
    public List<TaskEntity> findAllByUserIdUnlinked(@NotNull String userId);

    public void removeByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    public List<TaskEntity> findTasksByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    public List<TaskEntity> findAllByUserIdUnlinkedSorted(@NotNull String userId, @NotNull String sqlSortType);

}
