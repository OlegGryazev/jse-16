package ru.gryazev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IEntityManagerService entityManagerService = new EntityManagerService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(entityManagerService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(entityManagerService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(entityManagerService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(entityManagerService);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint();

    public void init() throws Exception {
        projectEndpoint.setServiceLocator(this);
        taskEndpoint.setServiceLocator(this);
        userEndpoint.setServiceLocator(this);
        sessionEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/session?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/project?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/task?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/user?wsdl", userEndpoint);
    }

}